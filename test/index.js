// Import stylesheets
import './css/style.css';

// 0. Import LIFF SDK
import liff from '@line/liff';

// LiffId
const liffId = '1656452594-LezBR5Xx';

// Body element
const body = document.getElementById('body');

// Profile elements
const pictureUrl = document.getElementById('pictureUrl');
const userId = document.getElementById('userId');
const displayName = document.getElementById('displayName');
const statusMessage = document.getElementById('statusMessage');
const email = document.getElementById('email');

// OS
const os = document.getElementById('os');
const version = document.getElementById('version');

// Button elements
const btnShareMessage = document.getElementById('btnShareMessage');
const btnShareLocation = document.getElementById('btnShareLocation');
const btnScanCode = document.getElementById('btnScanCode');
const btnshareTargetPicker = document.getElementById('btnshareTargetPicker');
const btnSendMessage = document.getElementById('btnSendMessage');
const btnSendLocation = document.getElementById('btnSendLocation');

// Map
const x = document.getElementById('demo');

async function main() {
  liff.ready.then(() => {
    if (liff.getOS() === 'android') {
      //body.style.backgroundColor = '#888888';
    }

    if (liff.getOS() === 'ios') {
      //body.style.backgroundColor = '#cccccc';
    }

    if (liff.isInClient()) {
      getUserProfile();
    }

    os.innerHTML = '<b>OS: </b>' + liff.getOS();
    version.innerHTML = '<b>Line Version: </b>' + liff.getLineVersion();
    client.innerHTML = '<b>isInClient: </b>' + liff.isInClient();
    btnShareMessage.style.display = 'block';
    btnShareLocation.style.display = 'block';
    btnScanCode.style.display = 'block';
    btnSendMessage.style.display = 'block';
    btnSendLocation.style.display = 'block';
    btnshareTargetPicker.style.display = 'block';
  });

  await liff.init({ liffId: liffId });
}

main();

async function getUserProfile() {
  const profile = await liff.getProfile();

  pictureUrl.src = profile.pictureUrl;
  userId.innerHTML = '<b>userId: </b>' + profile.userId;
  displayName.innerHTML = '<b>displayName: </b>' + profile.displayName;
  statusMessage.innerHTML = '<b>statusMessage: </b>' + profile.statusMessage;
  email.innerHTML = '<b>email: </b>' + liff.getDecodedIDToken().email;
}

async function shareMessage() {
  const result = await liff.shareTargetPicker([
    {
      type: 'text',
      text: 'This msg was shared by LIFF',
    },
  ]);
  if (result) {
    alert('Msg was shared!');
  } else {
    alert('ShareTargetPicker was cancelled by user');
  }
  liff.closeWindow();
}

async function sendMessage() {
  const result = await liff
    .sendMessages([
      {
        type: 'text',
        text: 'Hello, World!',
      },
    ])
    .then(() => {
      console.log('message sent');
    })
    .catch((err) => {
      console.log('error', err);
    });
  liff.closeWindow();
}

async function sendLocation() {
  const result = await liff
    .sendMessages([
      {
        type: 'location',
        title: 'my location',
        address: '1-6-1 Yotsuya, Shinjuku-ku, Tokyo, 160-0004, Japan',
        latitude: 35.687574,
        longitude: 139.72922,
      },
      {
        type: 'text',
        text: 'https://maps.google.com/?ll=latitude,longitude',
      },
    ])
    .then(() => {
      console.log('message sent');
    })
    .catch((err) => {
      console.log('error', err);
    });
  liff.closeWindow();
}

async function shareLocation() {
  getLocation();

  const result = await liff.shareTargetPicker([
    {
      type: 'action',
      action: {
        type: 'location',
        label: 'Location',
      },
    },
  ]);
  if (result) {
    alert('Msg was shared!');
  } else {
    alert('ShareTargetPicker was cancelled by user');
  }
  liff.closeWindow();
}

async function ScanCode() {
  if (liff.isInClient()) {
    if (liff.scanCode) {
      liff.scanCode().then((result) => {
        // result = { value: '' }
      });
    }
  } else {
    alert('Mobile function only');
  }
}

async function sendTargetPicker() {
  const result = await liff.shareTargetPicker([
    {
      type: 'flex',
      altText: 'Flex Message',
      contents: {
        type: 'bubble',
        body: {
          type: 'box',
          layout: 'vertical',
          spacing: 'md',
          contents: [
            {
              type: 'text',
              text: "BROWN'S ADVENTURE",
              size: 'xl',
              gravity: 'center',
              weight: 'bold',
              wrap: true,
            },
            {
              type: 'box',
              layout: 'vertical',
              spacing: 'sm',
              margin: 'lg',
              contents: [
                {
                  type: 'box',
                  layout: 'baseline',
                  spacing: 'sm',
                  contents: [
                    {
                      type: 'text',
                      text: 'Date',
                      flex: 1,
                      size: 'sm',
                      color: '#AAAAAA',
                    },
                    {
                      type: 'text',
                      text: 'Monday 25, 9:00PM',
                      flex: 4,
                      size: 'sm',
                      color: '#666666',
                      wrap: true,
                    },
                  ],
                },
                {
                  type: 'box',
                  layout: 'baseline',
                  spacing: 'sm',
                  contents: [
                    {
                      type: 'text',
                      text: 'Place',
                      flex: 1,
                      size: 'sm',
                      color: '#AAAAAA',
                    },
                    {
                      type: 'text',
                      text: 'LINE Thailand',
                      flex: 4,
                      size: 'sm',
                      color: '#666666',
                      wrap: true,
                    },
                  ],
                },
                {
                  type: 'box',
                  layout: 'vertical',
                  margin: 'xxl',
                  contents: [
                    {
                      type: 'spacer',
                    },
                    {
                      type: 'image',
                      url: 'https://scdn.line-apps.com/n/channel_devcenter/img/fx/linecorp_code_withborder.png',
                      size: 'xl',
                      aspectMode: 'cover',
                    },
                    {
                      type: 'text',
                      text: 'You can enter the theater by using this code instead of a ticket',
                      margin: 'xxl',
                      size: 'xs',
                      color: '#AAAAAA',
                      wrap: true,
                    },
                  ],
                },
              ],
            },
          ],
        },
      },
    },
  ]);
  if (result) {
    alert(`[${result.status}] Message sent!`);
  } else {
    const [majorVer, minorVer, patchVer] = (liff.getLineVersion() || '').split(
      '.'
    );

    if (minorVer === undefined) {
      alert('ShareTargetPicker was canceled in external browser');
      return;
    }

    if (
      parseInt(majorVer) >= 10 &&
      parseInt(minorVer) >= 10 &&
      parseInt(patchVer) > 0
    ) {
      alert('ShareTargetPicker was canceled in LINE app');
    }
  }

  liff.closeWindow();
}

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    x.innerHTML = 'Geolocation is not supported by this browser.';
  }
}

function showPosition(position) {
  x.innerHTML =
    'Latitude: ' +
    position.coords.latitude +
    '<br>Longitude: ' +
    position.coords.longitude;

  var latlon = position.coords.latitude + ',' + position.coords.longitude;

  var img_url =
    'https://maps.googleapis.com/maps/api/staticmap?enter=' +
    latlon +
    '&zoom=14&size=400x300&sensor=false&key=YOUR_KEY';

  //document.getElementById('mappath').innerHTML = img_url;
}

//Event Handler
btnShareMessage.onclick = () => {
  shareMessage();
};

btnShareLocation.onclick = () => {
  shareLocation();
};
btnSendLocation.onclick = () => {
  sendLocation();
};

btnScanCode.onclick = () => {
  ScanCode();
};
btnshareTargetPicker.onclick = () => {
  sendTargetPicker();
};
btnSendMessage.onclick = () => {
  sendMessage();
};
